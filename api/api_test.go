package api

import (
	"os"
	"testing"
)

func getAPIKey() string {
	return os.Getenv("OMDBAPI_KEY")
}

func TestInvalidAPIKey(t *testing.T) {
	client := New("Invalid_Key")
	_, err := client.Posters("")

	if err == nil {
		t.Errorf("expected `%s` error got no errors", "Invalid API key!")
	}
}

func TestValidMovies(t *testing.T) {
	movies := []string{
		"The Shawshank Redemption",
		"The Godfather",
		"The Dark Knight",
		"12 Angry Men",
		"Schindler's List",
		"Pulp Fiction",
		"The Good, the Bad and the Ugly",
		"Fight Club",
	}

	client := New(getAPIKey())

	for _, movie := range movies {
		posters, err := client.Posters(movie)
		if err != nil {
			t.Errorf("expected no errors got %v", err)
		}

		if len(posters) == 0 {
			t.Error("expected poster or more got 0 posters")
		}
	}
}

func TestInValidMovies(t *testing.T) {
	movies := []string{
		"Invalid_name_1",
		"Invalid_name_2",
	}

	client := New(getAPIKey())

	for _, movie := range movies {
		posters, err := client.Posters(movie)
		if err == nil {
			t.Errorf("expected `%s` error got no errors", "Movie not found!")
		}

		if size := len(posters); size != 0 {
			t.Errorf("expected 0 posters got %d", size)
		}
	}
}
