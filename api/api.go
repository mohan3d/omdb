package api

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

const (
	apiVersion = "1"
	apiURL     = "https://www.omdbapi.com/"
)

type apiMovies struct {
	Search []struct {
		Title  string `json:"Title"`
		Year   string `json:"Year"`
		ImdbID string `json:"imdbID"`
		Type   string `json:"Type"`
		Poster string `json:"Poster"`
	} `json:"Search"`
	TotalResults string `json:"totalResults"`
	Response     string `json:"Response"`
}

type apiError struct {
	Response string `json:"Response"`
	Error    string `json:"Error"`
}

// OMDbAPI describes OMDb api client.
type OMDbAPI struct {
	apiKey string
}

// Posters returns matched movies posters urls.
func (api *OMDbAPI) Posters(movieTitle string) ([]string, error) {
	// create valid url with query params.
	URL, err := api.createURL(movieTitle)
	if err != nil {
		return nil, err
	}

	// request the api
	resp, err := http.Get(URL)
	if err != nil {
		return nil, err
	}

	// store response body, it will be used two times.
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	// check if the movie doesn't exist.
	if err = checkResponseError(data); err != nil {
		return nil, err
	}

	// parse all movies posters.
	var movies apiMovies
	var posters []string

	err = json.Unmarshal(data, &movies)
	if err != nil {
		return nil, err
	}

	for _, movie := range movies.Search {
		posters = append(posters, movie.Poster)
	}

	return posters, nil
}

func (api *OMDbAPI) createURL(movieTitle string) (string, error) {
	URL, err := url.Parse(apiURL)

	if err != nil {
		return "", err
	}
	query := URL.Query()
	query.Set("apikey", api.apiKey)
	query.Set("s", movieTitle)
	query.Set("v", apiVersion)
	URL.RawQuery = query.Encode()

	return URL.String(), nil
}

func checkResponseError(data []byte) error {
	var apiErr apiError
	err := json.Unmarshal(data, &apiErr)
	if err != nil {
		return err
	}
	if apiErr.Response == "False" {
		return errors.New(apiErr.Error)
	}
	return nil
}

// New creates new OMDbAPI object.
func New(apiKey string) *OMDbAPI {
	return &OMDbAPI{apiKey: apiKey}
}
