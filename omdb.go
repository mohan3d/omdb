package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strings"

	"bitbucket.org/mohan3d/omdb/api"
)

func downloadPoster(posterURL string) {
	filePath := path.Base(posterURL)
	file, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	resp, err := http.Get(posterURL)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		panic(err)
	}
}

func downloadPosters(posterURLs []string) {
	for _, posterURL := range posterURLs {
		if posterURL != "N/A" {
			downloadPoster(posterURL)
		}
	}
}

func main() {
	movieName := strings.TrimSpace(strings.Join(os.Args[1:], " "))
	apiKey := os.Getenv("OMDBAPI_KEY")

	if movieName == "" {
		fmt.Printf("Usage: %s <movieName>\n", os.Args[0])
		os.Exit(1)
	}

	if apiKey == "" {
		fmt.Printf("OMDBAPI_KEY must be exported before running %s\n", os.Args[0])
		os.Exit(1)
	}

	omdbapi := api.New(apiKey)
	postersURLs, err := omdbapi.Posters(movieName)

	if err != nil {
		panic(err)
	}

	downloadPosters(postersURLs)
}
