# omdb

Golang tool to download posters from [omdbapi](https://www.omdbapi.com/).

A solution for [**Exercise 4.13**](https://books.google.com.eg/books?redir_esc=y&id=SJHvCgAAQBAJ&q=Exercise+4.13#v=snippet&q=Exercise%204.13&f=false) in [**The Go Programming Language**](https://www.amazon.com/Programming-Language-Addison-Wesley-Professional-Computing/dp/0134190440).

## Installation
```bash
$ go get bitbucket.org/mohan3d/omdb
$ cd $GOPATH/src/bitbucket.org/mohan3d/omdb
$ go install
```

## Usage
```bash
$ export export OMDBAPI_KEY=<YOUR_OMDBAPI_KEY>
$ omdb <movieName>
```

## Testing
**OMDBAPI_KEY** must be exported to environment variables before running tests.

```bash
$ export export OMDBAPI_KEY=<YOUR_OMDBAPI_KEY>
```

